# Random Matrix device Utilities

Imagine you have a square physical matrix of unstable elements. If element states are visually distinct then entire matrix may be surrounded by a stable "finder" border, and simulate one of these popular [Data Matrix](https://en.wikipedia.org/wiki/Data_Matrix) patterns:

![Data Matrix pattern example](https://upload.wikimedia.org/wikipedia/commons/1/10/Matrix-46.PNG)

Though it doesn't form a valid encoding (because all correction bits are not set properly), it still represents a machine-readable form of data. If you can extract this raw random data, you have a pure hardware Random Number Generator. So this is a purpose of the Random Matrix device Utilities, derived from original [libtmtx Utils](https://github.com/dmtx/dmtx-utils) - read raw device message from a photo. And, optionally, condence it's entropy, if you wish to emit a proper strong crypto key from your trusted device.