#include "dmtxpack.h"

char *programName;

/**
 * @brief  Main function for the dmtxread Data Matrix scanning utility.
 * @param  argc count of arguments passed from command line
 * @param  argv list of argument passed strings from command line
 * @return Numeric exit code
 */
int
main(int argc, char *argv[])
{
    char octets[10000];
    int octets_count = fread(&octets, sizeof(char), sizeof(octets), stdin);
    int bit_field_size = octets_count * 8; // TODO read option

    int n_bits = 0;
    int layout[bit_field_size];
 
    int lastIndex = -1; 
    for (int j = 0; j < bit_field_size; j++)
        if (octets[j/8] & (0x01 << j % 8)) {
            layout[n_bits++] = j - lastIndex - 1;
            lastIndex = j;
        }
    
    mpz_t index;
    mpz_init(index);
    GetLayoutIndex(index, &layout[0], n_bits, bit_field_size);

    fwrite(index->_mp_d, sizeof(mp_limb_t), index->_mp_size, stdout);
    fflush(stdout);

    mpz_clear(index);
}

static void 
GetLayoutIndex(mpz_ptr index, int *layout, int k, int n) 
{
   if (k == 1)
      mpz_set_ui(index, (long)layout[0]);
   else {
      mpz_set_ui(index, 0L);

      mpz_t x;
      mpz_init(x);
         
      for (int d = 0; d < layout[0]; d++) {
         BinCoeff(x, n - d - 1, k - 1);
         mpz_add(index, index, x);
      }

      GetLayoutIndex(x, &layout[1], k - 1, n - layout[0] - 1);
      mpz_add(index, index, x);
      
      mpz_clear(x);
   }
}

static void
BinCoeff(mpz_ptr coeff, int n, int k) 
{
   mpz_set_ui(coeff, 1L);

   long i;
   for (i = n; i > n - k; i--)
      mpz_mul_ui(coeff, coeff, i);

   for (i = k; i > 1; i--)
      mpz_divexact_ui(coeff, coeff, i);
}
