
#ifndef __DMTXPACK_H__
#define __DMTXPACK_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include <gmp.h>

static void GetLayoutIndex(mpz_ptr index, int *layout, int k, int n);
static void BinCoeff(mpz_ptr coeff, int n, int k);
#endif